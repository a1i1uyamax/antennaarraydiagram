﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using NoizeFilter;

namespace AntennaArrayDiagram
{
    /// <summary>
    /// Логика взаимодействия для Window3D.xaml
    /// </summary>
    public partial class Window3D : Window
    {
        public WindowParams WP { set; get; }
        public int Pwidth { set; get; }
        public int Pheight { set; get; }

        public double[,] I { set; get; }
        public YBytes[,] B { set; get; }

        public int A { set; get; }

        Point3D[,] p;
        Model3DGroup group;
        
        static readonly double angle = 9.5;
        static readonly double step = 0.45;
        static readonly Quaternion qrX = new Quaternion(new Vector3D(1.0, 0.0, 0.0), angle);
        static readonly Quaternion qrY = new Quaternion(new Vector3D(0.0, 1.0, 0.0), angle);
        static readonly Quaternion qrZ = new Quaternion(new Vector3D(0.0, 0.0, 1.0), angle);

        public Window3D()
        {
            InitializeComponent();
        }

        private double NewCoords(int i, bool IsWidth)
        {
            return (i - (IsWidth ? Pwidth : Pheight) / 2.0) / 100.0;
        }

        public void CreateModel()
        {
            var model = new ModelVisual3D();

            p = new Point3D[Pwidth, Pheight];
            for (int i = 0; i < Pwidth; i++)
            {
                for (int j = 0; j < Pheight; j++)
                {
                    I[i, j] *= WP.MSH;
                    p[i, j] =
                        new Point3D(NewCoords(i, true), NewCoords(j, false), I[i, j]);
                }
            }
            
            group = new Model3DGroup();
            for (int i = 0; i < Pwidth - A; i += A)
            {
                for (int j = 0; j < Pheight - A; j += A)
                {
                    var p0 = p[i, j];
                    var p1 = p[i + A, j];
                    var p2 = p[i + A, j + A];
                    var p3 = p[i, j + A];
                    drawTriangle(p0, p1, p2, B[i, j].ToColor());
                    drawTriangle(p2, p3, p0, B[i, j].ToColor());
                }
            }

            var p00 = new Point3D(1.0, -1.0, 0.0);
            var p01 = new Point3D(-1.0, -1.0, 0.0);
            var p02 = new Point3D(-1.0, 1.0, 0.0);
            var p03 = new Point3D(1.0, 1.0, 0.0);
            drawTriangle(p00, p01, p02, Colors.Black);
            drawTriangle(p02, p03, p00, Colors.Black);

            mv3d.Content = group;
        }

        public void drawTriangle(Point3D p0, Point3D p1, Point3D p2, Color color)
        {
            var mesh = new MeshGeometry3D();

            mesh.Positions.Add(p0);
            mesh.Positions.Add(p1);
            mesh.Positions.Add(p2);
            
            mesh.TriangleIndices.Add(0);
            mesh.TriangleIndices.Add(1);
            mesh.TriangleIndices.Add(2);

            Material material = 
                new DiffuseMaterial(new SolidColorBrush(color));
            
            group.Children.Add(new GeometryModel3D(mesh, material));
        }

        private Vector3D CreateNormal(Point3D p0, Point3D p1, Point3D p2)
        {
            Vector3D v0 = new Vector3D(p1.X - p0.X, p1.Y - p0.Y, p1.Z - p0.Z);
            Vector3D v1 = new Vector3D(p2.X - p1.X, p2.Y - p1.Y, p2.Z - p1.Z);
            return Vector3D.CrossProduct(v0, v1);
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.A:
                    rotation.Quaternion = qrY.Conjurgating() * rotation.Quaternion;
                    break;

                case Key.D:
                    rotation.Quaternion = qrY * rotation.Quaternion;
                    break;

                case Key.W:
                    rotation.Quaternion = qrX.Conjurgating() * rotation.Quaternion;
                    break;

                case Key.S:
                    rotation.Quaternion = qrX * rotation.Quaternion;
                    break;

                case Key.Left:
                    rotation.Quaternion = qrZ * rotation.Quaternion;
                    break;

                case Key.Right:
                    rotation.Quaternion = qrZ.Conjurgating() * rotation.Quaternion;
                    break;

                case Key.Up:
                    Cam.Position -= 
                        ((Vector3D)(Cam.Position - Cam.LookDirection)).Norm() * step;
                    break;

                case Key.Down:
                    Cam.Position +=
                        ((Vector3D)(Cam.Position - Cam.LookDirection)).Norm() * step;
                    break;
                default:
                    break;
            }
        }
        
    }
}
