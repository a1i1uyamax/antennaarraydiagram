﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace AntennaArrayDiagram
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int antennCount;
        private int N;
        private List<Point> antnns_coords;

        double dX;
        double dY;

        public int AntennCount => antennCount;
        public List<Point> ACoords => antnns_coords;
        
        public MainWindow()
        {
            InitializeComponent();
            N = 10;
            antennCount = 0;
            antnns_coords = new List<Point>();

        }
        
        private void MainGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            var div = new Point(dX, dY);
            var p = getMousePoint(div, out Point aCoords);

            if (p != new Point())
            {
                foreach (var item in antnns_coords)
                {
                    if (item.Equals(p)) return;
                }

                antnns_coords.Add(aCoords);
                
                Ellipse el = new Ellipse
                {
                    Width = dX,
                    Height = dY,
                    Stroke = Brushes.Brown,
                    StrokeThickness = 6.5,
                    Fill = Brushes.DarkOrange,
                    VerticalAlignment = VerticalAlignment.Top,
                    Margin = new Thickness(p.X, p.Y,
                        Width - p.X - 2.0 * dX,
                        Height - p.Y - 2.0 * dY)
                };

                MainGrid.Children.Add(el);
            }
        }

        private Point getMousePoint(Point div, out Point iCoords)
        {
            var nllblX = getMouseCoord(div, true, out double? iX);
            var nllblY = getMouseCoord(div, false, out double? iY);
            iCoords = new Point(iX.GetValueOrDefault(), iY.GetValueOrDefault());

            if (nllblX != null && nllblY != null)
            {
                ++antennCount;

                double X = (double)nllblX;
                double Y = (double)nllblY;
                
                return new Point(X, Y);
            }

            return new Point();
        }

        private double? getMouseCoord(Point div, bool isX, out double? ir)
        {
            double d = isX ? div.X : div.Y;
            double D = isX ? Mouse.GetPosition(MainGrid).X
                : Mouse.GetPosition(MainGrid).Y;
            for (int i = 0; i < N; i++)
            {
                if ((i + 1) * d > D)
                {
                    ir = i;
                    return i * d;
                }
            }
            ir = null;
            return null;
        }

        private void AADWindow_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (MainGrid.Children.Count != 0 && 
                    MainGrid.Children[MainGrid.Children.Count - 1] is Ellipse)
            {
                MainGrid.Children.RemoveAt(MainGrid.Children.Count - 1);
                antnns_coords.RemoveAt(antnns_coords.Count - 1);
                --antennCount;
            }
        }

        private void AADWindow_Loaded(object sender, RoutedEventArgs e)
        {
            dX = AADWindow.Width / (N + 1);
            dY = AADWindow.Height / (N + 1);

            for (int i = 0; i < 11; i++)
            {
                Line l1 = new Line
                {
                    X1 = 0.0,
                    Y1 = dY * i,
                    X2 = AADWindow.Width,
                    Y2 = dY * i,
                    Stroke = Brushes.YellowGreen
                };

                Line l2 = new Line
                {
                    X1 = dX * i + 11.0,
                    Y1 = 0.0,
                    X2 = dX * i + 11.0,
                    Y2 = AADWindow.Height,
                    Stroke = Brushes.YellowGreen
                };

                MainGrid.Children.Add(l1);
                MainGrid.Children.Add(l2);
            }

            WindowParams window = new WindowParams
            {
                Owner = this,
                MW = this,

            };

            window.Show();
        }
    }
}
