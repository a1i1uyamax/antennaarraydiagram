﻿using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using NoizeFilter;

namespace AntennaArrayDiagram
{
    /// <summary>
    /// Логика взаимодействия для Window2D.xaml
    /// </summary>
    public partial class Window2D : Window
    {
        public YBytes[,] b { set; get; }
        WriteableBitmap wrb;
        public WindowParams WP { set; get; }
        public int Pwidth { set; get; }
        public int Pheight { set; get; }
        public double[,] I { set; get; }

        public Window2D()
        {
            InitializeComponent();
        }
               

        public void SET()
        {
            double dpi = 1;
            wrb = new WriteableBitmap(Pwidth, Pheight,
                dpi, dpi, PixelFormats.Bgra32, null);

            int step = wrb.PixelWidth * wrb.Format.BitsPerPixel / 8;

            for (int i = 0; i < WP.pWidth; i++)
            {
                for (int j = 0; j < WP.pHeight; j++)
                {
                    wrb.WritePixels(new Int32Rect(i, j, 1, 1), b[i, j].ToARGB(), step, 0);
                }
            }

            MainImage.Source = wrb;
        }
    }
}
