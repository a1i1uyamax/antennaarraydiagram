﻿namespace System.Windows.Media.Media3D
{
    public static class DHelp
    {
        public static Vector3D Normal(this Vector3D v)
        {
            return v / v.Length;
        }

        public static Quaternion ToQuaternion(this Point3D p)
        {
            return new Quaternion(p.X, p.Y, p.Z, 0.0);
        }

        public static Quaternion ToQuaternion(this Vector3D p)
        {
            return new Quaternion(p.X, p.Y, p.Z, 0.0);
        }

        public static Point3D ToPoint3D(this Quaternion q)
        {
            return new Point3D(q.X, q.Y, q.Z);
        }

        public static Point3D Mul(this Point3D p, double d)
        {
            return new Point3D(p.X * d, p.Y * d, p.Z * d);
        }

        public static Vector3D ToVector3D(this Quaternion q)
        {
            return new Vector3D(q.X, q.Y, q.Z);
        }

        public static Vector3D Norm(this Vector3D p)
        {
            return p / p.Length;
        }

        public static Quaternion Conjurgating(this Quaternion q)
        {
            return new Quaternion(-q.X, -q.Y, -q.Z, q.W);
        }

        public static Quaternion Rotate(Quaternion q_orientation, Quaternion q_of_rotation)
        {
            return q_of_rotation * q_orientation * q_of_rotation.Conjurgating();
        }

        public static double Norm(this Quaternion q)
        {
            return q.X * q.X + q.Y * q.Y + q.Z * q.Z + q.W * q.W;
        }

        public static Quaternion Normal(this Quaternion q)
        {
            if (q.Module() != 0.0) return q.Div(q.Module());

            return new Quaternion(0.0, 0.0, 0.0, 1.0);
        }

        public static double Module(this Quaternion q)
        {
            return Math.Sqrt(q.Norm());
        }

        public static Quaternion Div(this Quaternion q, double d)
        {
            return new Quaternion(q.X / d, q.Y / d, q.Z / d, q.W / d);
        }

        public static Quaternion Inverse(this Quaternion q)
        {
            return q.Conjurgating().Div(q.Norm());
        }
    }
}
