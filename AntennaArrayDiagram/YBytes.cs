﻿using System.Windows.Media;

namespace NoizeFilter
{
    public class YBytes
    {
        public byte Y { get; set; }

        public YBytes(byte Y)
        {
            this.Y = Y;
        }

        public YBytes(byte[] b, bool chb = false)
        {
            if (chb)
            {
                double a = 0.299 * b[0] + 0.587 * b[1] + 0.144 * b[2];
                if (a > 255.0) Y = 255;
                else if (a < 0.0) Y = 0;
                else Y = (byte)a;
            }
            else Y = b[2];
        }

        public YBytes() { }

        public byte[] ToARGB()
        {
            return new byte[4] { Y, Y, Y, 255 };
        }

        public override string ToString()
        {
            return Y.ToString();
        }

        public Color ToColor()
        {
            return new Color
            {
                A = 255,
                B = Y,
                G = Y,
                R = Y
            };
        }
    }
}
