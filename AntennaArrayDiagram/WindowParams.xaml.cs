﻿using NoizeFilter;
using System;
using System.Windows;

namespace AntennaArrayDiagram
{
    /// <summary>
    /// Логика взаимодействия для WindowParams.xaml
    /// </summary>
    public partial class WindowParams : Window
    {
        public MainWindow MW { set; get; }

        double width = 200.0;
        double height = 200.0;
        //double R;

        public double pWidth => width;
        public double pHeight => height;
        public double MSH { set; get; }

        double[,] I;
        public double Max { set; get; }
        YBytes[,] b;

        public WindowParams()
        {
            InitializeComponent();
        }

        private void Bttn_3D_Click(object sender, RoutedEventArgs e)
        {
            CalcByProjections();

            int A = int.Parse(Tb_A.Text);

            Window3D w = new Window3D
            {
                Owner = this,
                WP = this,
                Pwidth = (int)width,
                Pheight = (int)height,
                I = I,
                B = b,
                A = A
            };

            w.CreateModel();

            w.ShowDialog();
        }

        private void Bttn_2D_Click(object sender, RoutedEventArgs e)
        {
            CalcByProjections();

            Window2D w = new Window2D
            {
                Owner = this,
                WP = this,
                Pwidth = (int)width,
                Pheight = (int)height,
                I = I,
                b = b
            };

            w.SET();
            
            w.ShowDialog();
        }

        void CalcByProjections()
        {
            var coef = double.Parse(Tb_Coef.Text);

            var R = double.Parse(Tb_R.Text) / coef;
            var d = double.Parse(Tb_d.Text) / coef;
            var lam = double.Parse(Tb_Lam.Text) / coef;
            var list = MW.ACoords;
            MSH = double.Parse(Tb_MSH.Text);

            for (int i = 0; i < list.Count; i++)
            {
                list[i] -= -new Vector(5.0, 5.0);
            }

            int w = (int)width, h = (int)height;
            I = new double[w, h];

            var c = new Point(w / 2.0, h / 2.0);

            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < h; j++)
                {
                    double ci = i - c.X;
                    double cj = j - c.Y;

                    if (R * R > ci * ci + cj * cj)
                    {
                        double sqrZp = R * R - ci * ci - cj * cj;
                        Complex sum = 0.0;
                        for (int k = 0; k < list.Count; k++)
                        {
                            double ri = Math.Sqrt((ci - list[k].X * d) * (ci - list[k].X * d)
                                + (cj - list[k].Y * d) * (cj - list[k].Y * d) + sqrZp);

                            sum += Complex.Exp(Complex.J * 2.0 * Math.PI * ri / lam) / ri;
                        }
                        double abs = sum.Abs;
                        I[i, j] = Math.Sqrt(abs * abs);
                    }
                }
            }

            Max = 0.0;
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    if (I[i, j] > Max) Max = I[i, j];
                }
            }

            b = new YBytes[(int)width, (int)height];

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    b[i, j] = new YBytes((byte)(I[i, j] / Max * 254.5));
                }
            }
        }
        
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MW.Close();
        }

        //public static Point Mul(Point p, double d)
        //{
        //    return new Point(p.X * d, p.Y * d);
        //}
    }
}
